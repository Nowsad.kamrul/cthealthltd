
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Forms</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           <!--  <?php var_dump($user)?> -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create User :
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="<?php echo site_url('auth/edit_user/'.$user->id);?>" method="post">
                                        <div class="form-group">
                                            <label>First Name</label>
                                           
                                           <input class="form-control" type="text" name="first_name" placeholder="Enter Your First Name" value="<?php echo $user->first_name?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input class="form-control" type="text" name="last_name" placeholder="Enter Your Last Name" value="<?php echo $user->last_name?>">
                                        </div>
                                        
                                          <div class="form-group">
                                            <label>Company</label>
                                           <input class="form-control" type="text" name="company" value="<?php echo $user->company?>">
                                        </div>
                                        
                                         
                                        
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input class="form-control" type="text" name="phone" value="<?php echo $user->phone?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" type="email" name="email" value="<?php echo $user->email?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control" type="password" name="password" placeholder="Password Must be 8 characters long">
                                        </div>
                                       
                                       <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input class="form-control" type="password" name="password_confirm"placeholder="Repeat Your Password">
                                        </div>
                                       
                                       
                                        <button type="submit" class="btn btn-default">Update</button>
                                       
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>