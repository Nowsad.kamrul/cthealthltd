<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserAdmin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
	}

	public function index()
	{
		
	$this->load->view('pages/login');
	}


	public function loadDashboard()
	{
		$data['contain']='dashboard';
		$this->load->view('includes/template',$data);
	}

public function loadForm()
	{
		$data['contain']='pages/form';
		$this->load->view('includes/template',$data);

	}
public function loadTable()
	{
		$data['contain']='pages/table';
		$this->load->view('includes/template',$data);
	}
    public function userList()
    {
    	$data['users'] = $this->ion_auth->users()->result_array();
			
			$data['contain']='pages/table';
		$this->load->view('includes/template',$data);


    }
 //    public function editUser($id)
	// {
	// 	$data['contain']='pages/editform';
	// 	$this->load->view('includes/template',$data);
	// }
	public function deleteUser($id)


	{   $this->load->model('user_model');
		$data['users'] =$this->user_model->deleteUser($id);
		// $data['contain']='pages/table';
		// $this->load->view('includes/template',$data);
		     redirect('userAdmin/userList');
	}


       
  


}





