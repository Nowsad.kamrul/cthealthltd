<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {
	
	
	public function index()
	{
		$this->load->view('welcome');
	}
	public function formload()
	{
		$this->load->view('registrationform');
	}



	public function saveStudent(){

		$post=$this->input->post();

		$data=array(

			'fname'=>$this->input->post('fname'),
			'lname'=>$post['lname'],
			'student_id'=>$post['student_id'],
			'gender'=>$post['gender'],
			'blood'=>$post['blood'],
			'address'=>$post['address'],
			'mobile'=>$post['mobile'],
			'email'=>$post['email']
			);
		$this->load->model('student_model');

		$this->student_model->save($data);

		redirect('student/viewStudent');
	}
	public function viewStudent(){
		$this->load->model('student_model');
		$data['students'] =$this->student_model->getStudent();
		//var_dump($data['students']);die;
		$this->load->view('show',$data);

	}

	public function editStudent($id){

       $this->load->model('student_model');
       $data['student'] =$this->student_model->updateStudent($id);
    
       $this->load->view('edit',$data);

	}
	public function deleteStudent($id){

       $this->load->model('student_model');
       $data['students'] =$this->student_model->deleteStudent($id);
       redirect('student/viewStudent');
      


	}
	public function updateStudent($id){

		$post=$this->input->post();
		
		$data=array(

			'fname'=>$this->input->post('fname'),
			'lname'=>$post['lname'],
			'student_id'=>$post['student_id'],
			'gender'=>$post['gender'],
			'blood'=>$post['blood'],
			'address'=>$post['address'],
			'mobile'=>$post['mobile'],
			'email'=>$post['email']
			);
		$this->load->model('student_model');
       $data['student'] =$this->student_model->updatedStudent($data,$id);

        redirect('student/viewStudent');

	}


}