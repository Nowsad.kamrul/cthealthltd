<?php

class Student_model extends CI_Model {
  
  public function save($data){

  	$this->db->insert('studentinfo',$data);

  }
  public function getStudent(){
  	return $this->db->get('studentinfo')->result_array();
  }
 
public function updateStudent($id){

  return $this->db->where('id',$id)->get('studentinfo')->row_array();
     

}
public function deleteStudent($id){

$this->db->where('id',$id)->delete('studentinfo');

	
}

public function updatedStudent($data,$id){
	$this->db->where('id',$id)->update('studentinfo',$data);
}


 }