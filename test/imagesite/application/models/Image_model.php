
<?php

class Image_model extends CI_Model {
  
  public function save($data){

  	$this->db->insert('image',$data);

  }
  public function getImage(){

  	return $this->db->get('image')->result_array();
  }
 
public function updateImage($id){

  return $this->db->where('id',$id)->get('image')->row_array();
     

}
public function updatedImage($data,$id){
	$this->db->where('id',$id)->update('image',$data);
}
public function delete($id){

   $this->db->where('id',$id)->delete('image');

}


 }