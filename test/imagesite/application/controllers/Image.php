<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends CI_Controller {
    
   public function __construct() {
		parent::__construct();

		
	}

	 
	public function index()
	{
		$this->load->view('welcome');
	}

	public function imageload()
	{

  		$this->load->view('imageform');

	}
	public function saveImage(){

		

	




		if (!empty($_FILES['image']['name'])){

			$config['upload_path']='./image/';
			$config['allowed_types']='jpg|png|gif|bmp';
			$config['max_size']='10000';
		$this->load->library('upload');
			$this->upload->initialize($config);

			if ($this->upload->do_upload("image")){
				$image_data=$this->upload->data();
				$post['image']=$image_data['file_name'];


			}else{


				echo $this->upload->display_errors();

				echo "Something going wrong ..retry";
			}

		}else{

		 $post['image']='';
	   }
	   $data = array('name' => $this->input->post('name'), 'image' =>$post['image']);
	   	$this->load->model('image_model');
		$this->image_model->save($data);

		redirect('image/showImage');

   }

public function showImage(){


		$this->load->model('image_model');
		$data['images'] =$this->image_model->getImage();
		//var_dump($data['images']);die;
		$this->load->view('show',$data);

}


public function editImage($id){

       $this->load->model('image_model');
       $data['image'] =$this->image_model->updateImage($id);
    
       $this->load->view('edit',$data);

	}
public function updateImage($id){




		if (!empty($_FILES['image']['name'])){


			$config['upload_path']='./image/';
			$config['allowed_types']='jpg|png|gif|bmp';
			$config['max_size']='10000';
		$this->load->library('upload');
			$this->upload->initialize($config);

			if ($this->upload->do_upload("image")){
				$image_data=$this->upload->data();
				$image=$image_data['file_name'];
				unlink('./image/'.$this->input->post('oldimage'));
            $data = array('name' => $this->input->post('name'),'image'=>$image);
            
			}else{


				echo $this->upload->display_errors();

				echo "Something going wrong ..retry";
			}

		}else{

		
         $data = array('name' => $this->input->post('name'));
	   }
	 
	   
	  
	   	$this->load->model('image_model');
		$this->image_model->updatedImage($data,$id);

		redirect('image/showImage');



}

public function deleteImage($id,$image){

	unlink('./image/'.$image);
	 $this->load->model('image_model');
	 $this->image_model->delete($id);
	 redirect('image/showImage');





}




}