<?php

class Employee_model extends CI_Model {
  
  public function save($data){

  	$this->db->insert('employeeinfo',$data);

  }
  public function getEmployee(){
  	return $this->db->get('employeeinfo')->result_array();
  }
 
public function updateEmployee($id){

  return $this->db->where('id',$id)->get('employeeinfo')->row_array();
     

}
public function deleteEmployee($id){

$this->db->where('id',$id)->delete('employeeinfo');

	
}

public function updatedEmployee($data,$id){
	$this->db->where('id',$id)->update('employeeinfo',$data);
}


 }