<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
	
	
	public function index()
	{
		$this->load->view('form');
	}

	public function saveEmployee(){

		$post=$this->input->post();

		$data=array(

			'fname'=>$this->input->post('fname'),
			'lname'=>$post['lname'],
			'emp_id'=>$post['emp_id'],
			'gender'=>$post['sex'],
			'blood'=>$post['blood'],
			'address'=>$post['address'],
			'mobile'=>$post['mobile'],
			'email'=>$post['email']
			);
		$this->load->model('employee_model');

		$this->employee_model->save($data);

		redirect('employee/viewEmployee');
	}
	public function viewEmployee(){
		$this->load->model('employee_model');
		$data['employees'] =$this->employee_model->getEmployee();
		//var_dump($data['employees']);die;
		$this->load->view('show',$data);

	}

	public function editEmployee($id){

       $this->load->model('employee_model');
       $data['employee'] =$this->employee_model->updateEmployee($id);
       //var_dump($data['employee']);die;
      // redirect('employee/edit');
       $this->load->view('edit',$data);

	}
	public function deleteEmployee($id){

       $this->load->model('employee_model');
       $data['employees'] =$this->employee_model->deleteEmployee($id);
       redirect('employee/viewEmployee');


	}
	public function updateEmployee($id){

		$post=$this->input->post();
		
		$data=array(

			'fname'=>$this->input->post('fname'),
			'lname'=>$post['lname'],
			'emp_id'=>$post['emp_id'],
			'gender'=>$post['sex'],
			'blood'=>$post['blood'],
			'address'=>$post['address'],
			'mobile'=>$post['mobile'],
			'email'=>$post['email']
			);
		$this->load->model('employee_model');
       $data['employee'] =$this->employee_model->updatedEmployee($data,$id);

        redirect('employee/viewEmployee');

	}


}