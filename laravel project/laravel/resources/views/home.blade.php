@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">


                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">


                            </li>
                            <li>
                                <a href={{ url('/home') }}><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">

                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href={{ url('/tables') }}><i class="fa fa-table fa-fw"></i> Tables</a>
                            </li>
                            <li>
                                <a href={{ url('/forms') }}><i class="fa fa-edit fa-fw"></i> Forms</a>
                            </li>

                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
