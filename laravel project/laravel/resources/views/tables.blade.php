<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tables</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    DataTables Advanced Tables
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Company</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>



                            </tr>
                            </thead>

{{--
                            <tbody>
                           --}}{{-- <?php foreach($users as $data){?>--}}{{--

                            <tr>

                                <td><?php echo ucwords($data['first_name']); ?></td>
                                <td><?php echo ucwords($data['last_name']); ?></td>
                                <td><?php echo $data['company']?></td>
                                <td><?php echo $data['email']?></td>
                                <td><?php echo $data['phone']?></td>
                                <td><a href="<?php echo site_url('auth/edit_user/'.$data['id'])?>">Update</a> | <a href="<?php echo site_url('userAdmin/deleteUser/'.$data['id'])?>">delete</a></td>

                            </tr>

                          --}}{{--  <?php         }   ?>--}}{{--
                            </tbody>--}}
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
